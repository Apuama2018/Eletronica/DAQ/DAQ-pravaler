#include <Wire.h>
#include <max6675.h>
#include <SPI.h>
#include <SD.h>

#define SLAVE_ADDRESS 0x04
int number = 0;
int state = 0;
int ktcSO = 5;
int ktcCS = 6;
int ktcCLK = 7;
const int chipSelect = 7;
File dataFile;

int x;

MAX6675 ktc(ktcCLK, ktcCS, ktcSO);

void setup() {
     //x = ktc.readCelsius();
  
pinMode(13, OUTPUT);
Serial.begin(9600); // start serial for output
// initialize i2c as slave
Wire.begin(SLAVE_ADDRESS);

// define callbacks for i2c communication
Wire.onReceive(receiveData);
Wire.onRequest(sendData);

Serial.println("Ready!");
// see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    return;
  }
  Serial.println("card initialized.");
}

void loop() {
  delay(100);
  dataFile = SD.open("datalog.csv", FILE_WRITE);
  // basic readout test
    x = ktc.readCelsius();
   Serial.print("Deg C = "); 

   Serial.print(x);
   Serial.print("\t Deg F = ");
   Serial.println(ktc.readFahrenheit());

   if (dataFile) {
    dataFile.println(ktc.readCelsius());
    dataFile.println(ktc.readFahrenheit());
    // close the file:
    dataFile.close();
    Serial.println("done.");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
 
   delay(500);
}

// callback for received data
void receiveData(int byteCount){

while(Wire.available()) {
number = Wire.read();
Serial.print("data received: ");
Serial.println("LuizGay");

//if (number == 1){

//if (state == 0){
//digitalWrite(13, HIGH); // set the LED on
//state = 1;
//}
//else{
//digitalWrite(13, LOW); // set the LED off
//state = 0;
//}
//}
}
}

// callback for sending data
void sendData(){
//number = ktc.readCelsius();
Wire.write(x);
}
